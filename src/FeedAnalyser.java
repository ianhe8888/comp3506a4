import java.util.*;

/**
 * Class that implements the social media feed searches
 */
public class FeedAnalyser {
    private  List<FeedItem> itemList;
    private  Stack<FeedItem> itemStacker = new Stack<>();
    private int count = 0;

    /**
     * Loads social media feed data from a file
     *
     * @param filename the file to load from
     */
    public FeedAnalyser(String filename) {
        Iterator<FeedItem> iter = new Util.FileIterator(filename);
        itemList = new ArrayList<>();
        while (iter.hasNext()) {
            FeedItem item = iter.next();
            itemList.add(item);
        }
    }

    /**
     * Return all feed items posted by the given username between startDate and endDate (inclusive)
     * If startDate is null, items from the beginning of the history should be included
     * If endDate is null, items until the end of the history should be included
     * The resulting list should be ordered by the date of each FeedItem
     * If no items that meet the criteria can be found, the empty list should be returned
     *
     * @param username  the user to search the posts of
     * @param startDate the date to start searching from
     * @param endDate   the date to stop searching at
     * @return a list of FeedItems made by username between startDate and endDate
     * @require username != null
     * @ensure result != null
     */
    public List<FeedItem> getPostsBetweenDates(String username, Date startDate, Date endDate) {
        List<FeedItem> tempList;
        tempList = getDatesHelper(username, startDate, endDate);
        dateSortHelper(tempList);
        return tempList;
    }

    /**
     * Return the first feed item posted by the given username at or after searchDate
     * That is, the feed item closest to searchDate that is greater than or equal to searchDate
     * If no items that meet the criteria can be found, null should be returned
     *
     * @param username   the user to search the posts of
     * @param searchDate the date to start searching from
     * @return the first FeedItem made by username at or after searchDate
     * @require username != null && searchDate != null
     */
    public FeedItem getPostAfterDate(String username, Date searchDate) {
        int count = 0;
        FeedItem min = null;
        for (FeedItem feedItem : itemList) {
            if (feedItem.getUsername().equals(username)
                    && feedItem.getDate().compareTo(searchDate) >= 0) {//if date and user name match
                if (count == 0) {//only run first time for the initial data
                    min = feedItem;
                    count++;
                } else {
                    if (dateCompare(min, feedItem) > 0) {//if second item is greater than first
                        // item, first item = second item
                        min = feedItem;
                    }
                }
            }
        }
        return min;
    }

    /**
     * Return the feed item with the highest upvote
     * Subsequent calls should return the next highest item
     * i.e. the nth call to this method should return the item with the nth highest upvote
     * Posts with equal upvote counts can be returned in any order
     *
     * @return the feed item with the nth highest upvote value,
     * where n is the number of calls to this method
     * @throws NoSuchElementException if all items in the feed have already been returned
     *                                by this method
     */
    public FeedItem getHighestUpvote() throws NoSuchElementException {
        if (count == 0 ) {//this will only run once
            List<FeedItem> tempList = itemList;
            upVotesSortHelper(tempList);
            for (FeedItem item : tempList) {
                itemStacker.push(item);
            }
            count++;
        }
        if (itemStacker.isEmpty()) {
            throw new NoSuchElementException();
        }
        return itemStacker.pop();
    }

    /**
     * Return all feed items containing the specific pattern in the content field
     * Case should not be ignored, eg. the pattern "hi" should not be matched in the text "Hi there"
     * The resulting list should be ordered by FeedItem ID
     * If the pattern cannot be matched in any content fields the empty list should be returned
     *
     * @param pattern the substring pattern to search for
     * @return all feed items containing the pattern string
     * @require pattern != null && pattern.length() > 0
     * @ensure result != null
     */
    public List<FeedItem> getPostsWithText(String pattern) {
        List<FeedItem> tempList = new ArrayList<>();
        for (FeedItem item : itemList) {
            if (item.getContent().contains(pattern)) {
                tempList.add(item);
            }
        }
        idSortHelper(tempList);
        return tempList;
    }

    //region Helper Function

    //region Date helper Function
    private List<FeedItem> getDatesHelper(String username, Date startDate, Date endDate) {
        int mode = 0;
        mode = getMode(startDate, endDate, mode);
        List<FeedItem> tempList = new ArrayList<>();
        for (FeedItem item : itemList) {
            if (item.getUsername().equals(username)) {
                addItemByMode(mode, tempList, item, startDate, endDate);
            }
        }
        return tempList;
    }

    private int getMode(Date startDate, Date endDate, int mode) {
        //both not null are 0, both null are 1, end are null are 2, start are null are 3
        if (startDate == null || endDate == null) {//both date are not null
            if (startDate == null && endDate == null) {//both null
                mode = 1;
            } else {
                if (endDate == null) {//end is null
                    mode = 2;
                } else {//start is null
                    mode = 3;
                }
            }
        }
        return mode;
    }

    private void addItemByMode(int mode, List<FeedItem> tempList, FeedItem item,
                               Date startDate, Date endDate) {
        //both not null are 0, both null are 1, end are null are 2, start are null are 3
        switch (mode) {
            case 0:
                if (endDate.compareTo(item.getDate()) >= 0
                        && startDate.compareTo(item.getDate()) <= 0) {
                    tempList.add(item);//mode0
                }
                break;
            case 1:
                tempList.add(item);
                break;
            case 2:
                if (startDate.compareTo(item.getDate()) <= 0) {
                    tempList.add(item);
                    break;
                }
            case 3:
                if (endDate.compareTo(item.getDate()) >= 0) {
                    tempList.add(item);
                }
                break;
        }
    }

    private void dateSortHelper(List<FeedItem> list) {
        if (!list.isEmpty()) {
            list.sort(new Comparator<FeedItem>() {
                @Override
                public int compare(FeedItem item1, FeedItem item2) {
                    return dateCompare(item1, item2);
                }
            });
        }
    }

    private int dateCompare(FeedItem item1, FeedItem item2) {
        return item1.getDate() == item2.getDate() ?//if date are equal, compare the id
                (int) ((item1.getDate().hashCode() + item1.getId()) -
                        (item2.getDate().hashCode() + item2.getId())) :
                item1.getDate().compareTo(item2.getDate());
    }

    //endregion

    //region upVote helper function
    private void upVotesSortHelper(List<FeedItem> list) {//insertion sort
        if (!list.isEmpty()) {
            list.sort(new Comparator<FeedItem>() {
                @Override
                public int compare(FeedItem item1, FeedItem item2) {
                    return upVotesCompare(item1, item2);
                }
            });
        }
    }

    private int upVotesCompare(FeedItem item1, FeedItem item2) {
        return item1.getUpvotes() == item2.getUpvotes() ?//if upvote are equal, compare the id
                (int) ((item1.getUpvotes() + item1.getId()) -
                        (item2.getUpvotes() + item2.getId())) :
                item1.getUpvotes() - item2.getUpvotes();
    }

    //endregion

    //region ID helper function
    private void idSortHelper(List<FeedItem> List) {
        if (!List.isEmpty()) {
            List.sort(new Comparator<FeedItem>() {
                @Override
                public int compare(FeedItem item1, FeedItem item2) {
                    return (int) (item1.getId() - item2.getId());
                }
            });
        }
    }
    //endregion
    //endregion
}
